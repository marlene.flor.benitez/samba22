#! /bin/bash

# crear usuaris unix
for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

# engegar serveis per fer el pam_ldap
/usr/sbin/nslcd
/usr/sbin/nscd

# Share public
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

# Creació usuaris ldap/samba
bash ./ldapusers-samba.sh
pdbedit -L

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"

# deixar el container interactiu
/bin/bash
